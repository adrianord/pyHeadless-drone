import numpy as np
import argparse
import cv2
import imutils
import math
from imutils.video import WebcamVideoStream
from imutils.video import FPS
from sys import stdout


global frame, roiPts, inputMode, frameName, objectxy, minArea,centerX,centerY,region,maxArea,activeBox, objects
objectxy = []
frame = None
roiPts = []
inputMode = False
frameName = "frame"
cachedPoints = {}
minArea = 1000
maxArea = 20000
objects = {}

def selectROI(event, x, y, flags, param):

    global frame, roiPts, inputMode, frameName
    if inputMode and event == cv2.EVENT_LBUTTONDOWN and len(roiPts) < 4:
		roiPts.append((x, y))
		cv2.circle(frame, (x, y), 4, (255, 0, 0), 2)
		cv2.imshow(frameName, frame)

def selectObject(event,x,y,flags,param):
    if inputMode and event == cv2.EVENT_LBUTTONDOWN and len(objectxy) < 1:
        objectxy.append((x,y))
        cv2.circle(frame, (x, y), 4, (255, 0, 0), 2)
        cv2.imshow(frameName, frame)

def distance(x1,x2,y1,y2):
    return math.sqrt(math.pow(x2 - x1,2) + math.pow(y2-y1,2))

def track(frameToAdd, frameToProcess):
    global minArea,centerX,centerY,region,maxArea,activeBox, objects
    cv2.rectangle(frameToAdd, (centerX-region, centerY-region),(centerX + region, centerY + region),(255,0,0),2)
    (_,cnts, _) = cv2.findContours(frameToProcess.copy(), cv2.RETR_LIST,cv2.CHAIN_APPROX_TC89_L1)
    detected = False
    for c in cnts:
        # if the contour is too small, ignore it
        if cv2.contourArea(c) < minArea or cv2.contourArea(c) > maxArea:
            continue

        # compute the bounding box for the contour, draw it on the frame,
        # and update the text
        (x, y, w, h) = cv2.boundingRect(c)



        if x > centerX - region and y > centerY - region and x + w < centerX + region and y + h < centerY + region:
            objCount = 0

            for key,value in objects.iteritems():
                if key[0] > x and key[1] > y and key[0] + key[2] < x + w and key[1] + key[3] < y + h:
                    objCount += 1
                if key[0] <= x and key[1] <= y and key[0] + key[2] >= x + w and key[1] + key[3] >= y + h:
                    objects[key] += 1

            objects.update({(x,y,w,h):objCount})
            boxX = x+(w/2)
            boxY = y+(h/2)
            cv2.circle(frameToAdd, (boxX, boxY), 4, (255, 0, 0), 2)
            if len(objectxy) > 0:
                (Ox,Oy) = objectxy[0]
                cv2.circle(frameToAdd, (Ox, Oy), 4, (0, 0, 255), 2)
                if activeBox is None:
                    activeBox = c
                    detected = True
                    # objectxy[0] = (boxX,boxY)
                else:
                    (Ax, Ay, Aw, Ah) = cv2.boundingRect(activeBox)
                    activeBoxX = Ax+(Aw/2)
                    activeBoxY = Ax+(Aw/2)
                    # activeBoxP = math.sqrt(math.floor(activeBoxX + Ox)) + math.sqrt(math.floor(activeBoxY + Oy))
                    # boxP = math.sqrt(math.floor(boxX + Ox)) + math.sqrt(math.floor(boxY + Oy))
                    activeBoxP = distance(Ox,activeBoxX,Oy,activeBoxY)
                    boxP = distance(Ox,boxX,Oy,boxY)
                    if boxP < activeBoxP:
                        activeBox = c
                        # objectxy[0] = (boxX,boxY)
                
            # else:
                # cv2.rectangle(frameToAdd, (x, y), (x + w, y + h), (0, 255, 0), 2)
                # cv2.drawContours(frameToAdd, [c],-1,(0,255,0),2)
    if detected is False:

        activeBox = None
    else:
        (x,y,w,h) = cv2.boundingRect(activeBox)
        cv2.rectangle(frameToAdd, (x, y), (x + w, y + h), (0, 0, 255), 2)

def main():
    global minArea,centerX,centerY,region,maxArea,activeBox, objects
    ap = argparse.ArgumentParser()

    print("[INFO] Waking webcam....")
    camera = WebcamVideoStream(src=0).start()
    # camera.stream.set(3,1920)
    # camera.stream.set(4,1080)
    fps = FPS().start()
    global frame, roiPts, inputMode, frameName
    activeBox = None
    timer = 0
    print("[INFO] Camera resolution: {:0f} x {:0f}".format(camera.stream.get(3),camera.stream.get(4)))
    cv2.namedWindow(frameName)
    cv2.setMouseCallback(frameName, selectObject)


    #termination criteria, look more into
    termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
    roiBox = None
    # previousFrame = camera.read()
    # previousFrame = imutils.resize(previousFrame,width=400)
    centerX = int(camera.stream.get(3)/2)
    centerY = int(camera.stream.get(4)/2)
    print("[INFO] Center X: {:2f} Center Y: {:2f}".format(centerX,centerY))
    lower = 100
    upper = 255
    region = 300
    while True:
        objects = {}
        frame = camera.read()
        cv2.rectangle(frame, (0,0),(640,480),(255,255,255),2)
        # frame = imutils.resize(frame,width = 400)
        
        fps.update()
        timer += 1
        if timer == 3: 
            orig = frame.copy()
            orig2 = frame.copy()
            orig3 = frame.copy()
            how = 13
            gray = orig.copy()
            gray = cv2.GaussianBlur(gray.copy(),(how,how),0)
            gray2 = gray.copy()
            gray = cv2.threshold(gray,lower,upper,cv2.THRESH_TOZERO)[1]
            gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
            gray = cv2.erode(gray,None,iterations=2)

            i = 255
            while(i > 0):
                work = gray2.copy()
                work = cv2.threshold(work,i,255,cv2.THRESH_BINARY)[1]
                work = cv2.cvtColor(work, cv2.COLOR_BGR2GRAY)
                work = cv2.erode(work,None,iterations=2)
                track(orig,work)
                work = gray2.copy()
                work = cv2.threshold(work,i,255,cv2.THRESH_BINARY_INV)[1]
                work = cv2.cvtColor(work, cv2.COLOR_BGR2GRAY)
                work = cv2.erode(work,None,iterations=2)
                track(orig,work)
                i = i - 40
            # print(objects)
            for key,value in objects.iteritems():
                if(value >=0):
                    (x,y,w,h) = key
                    cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # gray = cv2.dilate(gray,None,iterations=2)
            # previousGray = cv2.cvtColor(previousFrame, cv2.COLOR_BGR2GRAY)
            # previousFrame = frame.copy()
            # previousGray = cv2.GaussianBlur(previousGray,(how,how),0)
            # edged = cv2.Canny(gray2.copy(), 10, 20)
            # edgedPreview = edged.copy()
            text = "{} , {}, {}, {}".format(lower , upper, minArea, maxArea)
            # edgedil = cv2.dilate(edged.copy(),None,iterations=2)
            # mask = cv2.bitwise_not(edgedil.copy())
            # grayInv = cv2.bitwise_not(gray.copy())
            # edged = cv2.bitwise_and(edged,edged,mask=grayInv)
            # edged = cv2.dilate(edged.copy(),None,iterations=1)
            # edged = cv2.threshold(edged.copy(),0,255,cv2.THRESH_BINARY_INV)[1]
            # edged = cv2.threshold(gray.copy(),50,255,cv2.THRESH_BINARY)[1]
            # frameDelta = cv2.absdiff(previousGray,gray)
            # thresh = cv2.threshold(frameDelta, 35, 255, cv2.THRESH_BINARY)[1]
            # thresh = cv2.dilate(thresh, None, iterations=1)
            # both = cv2.bitwise_and(gray,gray,mask=mask)
            # both2 = cv2.add(both,edged,mask=None)
            # cv2.imshow("Difference",thresh)
            # track(orig3,gray)
            # track(orig2,edgedil)
            # track(orig,gray)
            # track(orig,edgedil)
            # toShow1 = np.hstack((orig,both))
            # toShow2 = np.hstack((orig2,both2))
            if roiBox is not None:
                backProj = cv2.calcBackProject([orig], [0], roiHist, [0, 180], 1)
                (r, roiBox) = cv2.CamShift(backProj, roiBox, termination)
                pts = np.int0(cv2.boxPoints(r))
                cv2.polylines(orig, [pts], True, (0, 255, 0), 2)

            # cv2.imshow("toShow1",orig3)
            # cv2.imshow("toShow2",orig2)
            cv2.imshow("Both",gray)
            # cv2.imshow("Both2",edgedil)
            cv2.putText(orig, text, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            # cv2.rectangle(orig, (centerX-region, centerY-region),(centerX + region, centerY + region),(255,0,0),2)
            cv2.imshow(frameName,orig)

            key = cv2.waitKey(1) & 0xFF
            timer = 0
            fps.stop()
            avgFps = "[INFO] average FPS: {} elasped time: {}".format(fps.fps(),fps.elapsed())
            # stdout.write("\r%s" % avgFps)
            # stdout.flush()
            fps = FPS().start()

            if key == ord("i") and len(objectxy) < 1:
                inputMode = True



                while len(objectxy) < 1:
                    cv2.imshow(frameName,orig)
                    cv2.waitKey(0)

            elif key == ord("q"):
                break
            elif key == ord("a"):
                lower -= 1
            elif key == ord("s"):
                lower += 1
            elif key == ord("z"):
                upper -= 10
            elif key == ord("x"):
                upper += 10
            elif key == ord("d"):
                minArea -= 50
            elif key == ord("f"):
                minArea += 50
            elif key == ord("c"):
                maxArea -= 500
            elif key == ord("v"):
                maxArea += 500
                
    fps.stop()
    print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
    print("[INFO] average FPS: {:.2f}".format(fps.fps()))
    cv2.destroyAllWindows()
    camera.stop()

if __name__ == "__main__":
	main()