from pyMultiwii import MultiWii
import msvcrt
import time
from sys import stdout

board = MultiWii("COM4")
data = [1500,1500,1200]
while 1:
    board.sendCMD(6,MultiWii.SET_MOTOR,data)
    time.sleep(0.09)
    x = msvcrt.kbhit()
    board.getData(MultiWii.MOTOR)
    statistics = "motor1 = {:+.2f} motor2 = {:+.2f} motor3 = {:+.2f} motor4 = {:+.2f} ".format(float(board.motor['m1']),float(board.motor['m2']),float(board.motor['m3']),float(board.motor['m4']))
    stdout.write("\r%s" % statistics )
    stdout.flush()
    if x: 
      ret = msvcrt.getch() 
    else: 
      ret = 0 
    if ret == "w":
        # print("pitch+")
        data[1] += 50
    elif ret == "s":
        # print("pitch-")
        data[1] -= 50
    elif ret == "a":
        # print("roll-")
        data[0] -= 100
    elif ret == "d":
        # print("roll+")
        data[0] += 100