import numpy as np
import cv2
import imutils
import time
import math
from threading import Thread
from sys import stdout
from pyMultiwii import MultiWii
from mosse import MOSSE
from common import RectSelector
from multiprocessing import Process, Pipe
import socket
from pyTCPServer import TCPServer

class ObjectTracker:

    def __init__(self, cameraId, show = True, gimbal = False, gimbalCom = "COM4"):
        self.cameraId = cameraId
        self.show = show
        self.gimbal = gimbal
        self.minArea = 300
        self.maxArea = 5000
        self.tracking = False
        self.frame = None
        self.objects = {}
        self.region = {"x":0,"y":0,"w":640,"h":480}
        self.center = {"x":self.region["w"]/2,"y":self.region["h"]/2}
        init = 150
        self.roi = {"x":(self.region["w"]/2) - init,"y":(self.region["h"]/2) - init,"w":(self.region["w"]/2) + init,"h":(self.region["h"]/2) + init}
        self.tracker = None
        cv2.namedWindow("Feed")
        self.rect_sel = RectSelector("Feed",self.onrect)
        self.TCP = TCPServer('192.168.0.77',8080)
        # cv2.setMouseCallback("Feed",self.selectROILocation)
        if gimbal:
            self.initGimbal(gimbalCom)
    

    def start(self):
        # p,c = Pipe()
        # Process(target=self.update,args=(p,self.cameraId)).start()
        t = Thread(target=self.updateGlobalsTCP)
        t.daemon = True
        t.start()
        while self.frame is None:
            stdout.write("\r%s" % "[INFO] Waking camera.....")
            stdout.flush()
        print("\n[INFO] Starting object tracking.....")
        # self.track()
        self.mosseTrack()

    def initGimbal(self,gimbalCom):
        print("[INFO] Waking gimbal.....")
        t = Thread(target=self.gimbalProc,args=(gimbalCom,self.center["x"],self.center["y"]))
        t.daemon = True
        t.start()

    def mosseTrack(self):
        start = time.clock()
        num = 0
        while True:
            frame = self.frame.copy()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            if self.tracker is not None:
                self.tracker.update(frame_gray)
            if self.show:
                vis = frame.copy()

                if self.tracker is not None:
                    self.tracker.draw_state(vis)
                    (x, y), (w, h) = self.tracker.pos, self.tracker.size
                    x1, y1, x2, y2 = int(x-0.5*w), int(y-0.5*h), int(x+0.5*w), int(y+0.5*h)
                    self.roi = {"x":x1,"y":y1,"w":x2,"h":y2}
                    cv2.rectangle(vis, (self.roi['x'], self.roi['y']), (self.roi['w'], self.roi['h'] ), (0, 255, 0), 2)
                    
                    if self.tracker.good == False:

                        self.tracker = MOSSE(frame_gray, (x1,y1,x2,y2))
                    self.tracking = True
                else:
                    self.tracking = False
                self.rect_sel.draw(vis)
                self.display(vis)
            num += 1
            end = time.clock()
            avg = end - start
            if(avg >= 0):
                avg = num / avg
                st = "[INFO] Frames Processed: {}".format(avg)
                if self.tracker:
                    st += ",Tracker: {}".format(self.tracker.good)
                stdout.write("\r%s" % st)
                # stdout.flush()
                num = 0
                start = time.clock()

    def onrect(self, rect):
        frame_gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        self.tracker = MOSSE(frame_gray, rect)
        # self.trackers.append(tracker)

    def track(self):
        start = time.clock()
        num = 0
        while True:
            self.objects = {}
            frame = self.frame.copy()
            roi = frame.copy()[self.roi["y"]:self.roi["h"],self.roi["x"]:self.roi["w"]]
            gaus = cv2.GaussianBlur(frame,(13,13),0)
            self.sweep(gaus[self.roi["y"]:self.roi["h"],self.roi["x"]:self.roi["w"]])
            self.singlular(frame)
            if self.show:
                self.display(frame)
                cv2.imshow("ROI",roi)
            num += 1
            end = time.clock()
            avg = end - start
            if(avg >= 0):
                avg = num / avg
                # stdout.write("\r%s" % "[INFO] Frames Processed: {} Tracked Object: {},{}".format(avg,self.roi["x"] + ((self.roi["w"]-self.roi["x"]) /2),self.roi["y"] + ((self.roi['h'] - self.roi["y"])/2)))
                stdout.flush()
                num = 0
                start = time.clock()
    

    def singlular(self,frame):
        cv2.rectangle(frame, (self.roi["x"], self.roi["y"]), ( self.roi["w"],  self.roi["h"]), (255, 0, 0), 2)
        if len(self.objects) > 0:
            oCount = 0
            avX = 0
            avY = 0
            avW = 0
            avH = 0
            for o in self.objects.keys():
                oCount += 1
                avX += o[0]
                avY += o[1]
                avW += o[0]+o[2]
                avH += o[1]+o[3]
            avX = avX / oCount+ self.roi["x"]
            avY = avY / oCount+ self.roi["y"]
            avW = avW / oCount+ self.roi["x"]
            avH = avH / oCount+ self.roi["y"]
            cv2.rectangle(frame, (avX, avY), (avW, avH ), (0, 0, 255), 2)
            out = 60
            if avX - out > 0 and avY - out > 0:
                self.tracking = True
                self.roi = {"x":avX - out,"y":avY - out,"w":avW + out,"h":avH + out}
        else:
            self.tracking = False

    def display(self,frame,green = True):
        for key,value in self.objects.iteritems():
            if value >=0 and green:
                (x,y,w,h) = key
                (x,y,w,h) = (x + self.roi["x"],y + self.roi["y"],w,h)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.imshow("Feed",frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('c'):
            self.tracker = None
            self.tracking = False

    def sweep(self,frame):
        #Seperate into a processing pool
        i = 255
        while(i > 0):
            try:
                work = frame.copy()
                work = cv2.threshold(work,i,255,cv2.THRESH_BINARY)[1]
                work = cv2.cvtColor(work, cv2.COLOR_BGR2GRAY)
                work = cv2.erode(work,None,iterations=2)
                self.find(work,self.roi)
                work = frame.copy()
                work = cv2.threshold(work,i,255,cv2.THRESH_BINARY_INV)[1]
                work = cv2.cvtColor(work, cv2.COLOR_BGR2GRAY)
                work = cv2.erode(work,None,iterations=2)
                self.find(work,self.roi)
            except:
                print("[WARNING] Frame dropped")
                pass
            i -= 50
    
    def find(self,frame,region):
        (_,cnts, _) = cv2.findContours(frame, cv2.RETR_LIST,cv2.CHAIN_APPROX_TC89_L1)
        for c in cnts:
            if cv2.contourArea(c) < self.minArea or cv2.contourArea(c) > self.maxArea:
                continue
            height, width = frame.shape
            (x, y, w, h) = cv2.boundingRect(c)
            if x < 10 or y < 10 or x + w > width - 10 or y + h > height - 10:
                continue
            # if x >= region["x"] and y >= region["y"] and x + w <= region["w"] and y + h <= region["h"]:
            objCount = 0
            for key,value in self.objects.iteritems():
                if key[0] > x and key[1] > y and key[0] + key[2] < x + w and key[1] + key[3] < y + h:
                    objCount += 1
                if key[0] <= x and key[1] <= y and key[0] + key[2] >= x + w and key[1] + key[3] >= y + h:
                    self.objects[key] += 1

            self.objects.update({(x,y,w,h):objCount})


    def updateGlobals(self,pipe):
        while(True):
            self.frame = pipe.recv()
    
    def updateGlobalsTCP(self):
        print "[INFO] Starting TCP Server...."
        self.TCP.listen()
        while(True):
            self.frame = self.TCP.recvframe()
            # cv2.imshow('SERVER',decimg)
            # cv2.waitKey(1) & 0xFF
        self.TCP.sock.close()


    def update(self,pipe,cameraId):
        print("[INFO] Capture Process Started.....")
        camera = cv2.VideoCapture(cameraId)
        while(True):
            (grabbed,frame) = camera.read()
            pipe.send(frame)

    def gimbalProc(self,gimbalCom,centerX,centerY):
        print("[INFO] Gimbal Process Starting.....")
        board = MultiWii(gimbalCom)
        data = [1500,1500,1200]
        x = 0
        y = 0
        print("[INFO] Gimbal Process Initialized.....")
        while True:
            board.sendCMD(6,MultiWii.SET_MOTOR,data)
            time.sleep(0.01)
            if self.tracking:
                y = self.roi["y"] + ((self.roi['h'] - self.roi["y"])/2)
                x = self.roi["x"] + ((self.roi["w"]-self.roi["x"]) /2)

                if abs(centerY - y) > 30:
                    mult = 1

                    if centerY - y < 0:
                        mult = -1

                    data[1] += mult * math.ceil(mult * (float(centerY) - float(y)) / 170.0)
                    
                
                if abs(centerX - x) > 25:
                    mult = 1

                    if centerX - x < 0:
                        mult = -1

                    data[0] = 1500 + mult * math.ceil(mult * (float(centerX) - float(x)) * 2.5)

                    if data[0] < 1000:
                        data[0] = 1050
                    elif data[0] > 2000:
                        data[0] = 1950

                    print("Tracking {},centerX {}, centerY {}, x {}, y {}, data {}, math {}".format(self.tracking,centerX,centerY,x,y,data,mult * math.ceil(mult * (float(centerX) - float(x)) / 10.0)))
                else:
                    data[0] = 1500
               
            else:
                data[0] = 1500
        

    def read(self):
        return self.frame
    
    def selectROILocation(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            init = 100
            x1 = x - init
            y1 = y - init
            if x1 < 0:
                x1 = 0
            if y1 < 0:
                y1 = 0
            self.roi = {"x": x1,"y": y1,"w": x + init,"h": y + init}

def main():
    camera = ObjectTracker(0,True,False)
    time.sleep(1)
    camera.start()

        

if __name__ == "__main__":
    main()