m= {0:1,1:1}
def fibm(n):
	m[n] = m[n] if n in m else fibm(n-1) + fibm(n-2)
	return m[n]

def fib(n):
	if n < 2:
		return n
	return fib(n-1) + fib(n-2)

if __name__ == "__main__":
	for x in range(0,20000):
		# print(fibm(x))
		# fibm(x)
		fib(x)
		if x % 10 is 0:
			print("{} finished".format(x))
	