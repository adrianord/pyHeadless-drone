from multiprocessing import Process, Pipe
import time
from threading import Thread


def f(conn):
    print("Started")
    rec = 1
    while rec < 100:
        conn.send(rec)
        rec += 1


if __name__ == '__main__':
    parent_conn, child_conn = Pipe()
    Process(target=f, args=(child_conn,)).start()
    rec = parent_conn.recv()
    while(rec < 99):
        rec = parent_conn.recv()
        print rec