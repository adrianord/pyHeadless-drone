import socket
import json as pickle

class UDPServer:

    def __init__(self, UDP_IP, UDP_PORT):
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.serversocket.bind((UDP_IP,UDP_PORT))
        print "{} is listening on port {}".format(UDP_IP,UDP_PORT)

    def nextCommand(self):
        message, addr = self.serversocket.recvfrom(1024)
        data_loaded = pickle.loads(message)
        # print message
        return data_loaded

class UDPClient:

    def __init__(self,UDP_IP,UDP_PORT):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.address = (UDP_IP, UDP_PORT)

    def sendCommand(self, message):
        self.sock.sendto(message,self.address)