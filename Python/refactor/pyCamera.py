import cv2
import imutils
import math
import numpy as np
import time
from multiprocessing import Process, Pipe
from threading import Thread

class Camera:

    def __init__(self, source):
        self.source = source
        self.frame = None

    def run(self):
        p,c = Pipe()
        Process(target=self.update,args=(p,self.source)).start()
        t = Thread(target=self.updateGlobals, args=(c,))
        t.daemon = True
        t.start()
        print "[INFO] Waking Camera {}.....".format(self.source)
        while self.frame is None:
            time.sleep(0.1)
        print "[INFO] Camera {} Started.....".format(self.source)
        
    def updateGlobals(self,pipe):
        while(True):
            self.frame = pipe.recv()

    def update(self,pipe,cameraId):
        print("[INFO] Capture Process Starting.....")
        camera = cv2.VideoCapture(cameraId)
        while(True):
            (grabbed,frame) = camera.read()
            pipe.send(frame)