from pyFlightController import FlightController
from pyTCPServer import TCPClient
from functools import partial
from threading import Thread
from pyCamera import Camera
from multiprocessing import Process, Pipe
from pyUDP import UDPServer
from pyLK import LK
from pyGimbal import Gimbal
from sys import stdout

class App:


    def __init__(self,COM_PORT,COM_PORT2,TCP_IP,TCP_PORT,UDP_IP, UDP_PORT):
        self.cam = Camera(1)
        self.cam.run()
        # self.bcam = Camera(0)
        # self.bcam.run()
        self.gimbal = Gimbal(COM_PORT2,self.cam)
        self.flight = FlightController(COM_PORT)
        # self.lk = LK(self.bcam,self.flight)
        self.tcp = TCPClient(TCP_IP,TCP_PORT)
        self.udp = UDPServer(UDP_IP,UDP_PORT)
        self.actions = {
            "w" : partial(self.flight.setData,FlightController.PITCH, 1),
            "s" : partial(self.flight.setData,FlightController.PITCH, -1),
            "d" : partial(self.flight.setData,FlightController.ROLL, 1),
            "a" : partial(self.flight.setData,FlightController.ROLL, -1),
            "e" : partial(self.flight.setData,FlightController.YAW, 1),
            "q" : partial(self.flight.setData,FlightController.YAW, -1),
            "r" : partial(self.flight.setData,FlightController.THROTTLE, 1),
            "f" : partial(self.flight.setData,FlightController.THROTTLE, -1),
            "z" : partial(self.flight.arm),
            "x" : partial(self.flight.disarm)
        }
        t0 = Thread(target=self.acceptCommands)
        t0.daemon = True
        t0.start()
        t1 = Thread(target=self.transmit)
        t1.daemon = True
        t1.start()
        t2 = Thread(target=self.gimbal.track)
        t2.daemon = True
        t2.start()
        t3 = Thread(target=self.showStatistics)
        t3.daemon = True
        t3.start()

    def transmit(self):
        print "[INFO] Transmittor service started!"
        while(True):
            self.tcp.sendFrame(self.cam.frame)

    def showStatistics(self):
        while True:
            data = self.flight.statistics(False,True,False,True)
            stdout.write("\r%s" % data)
            stdout.flush()

    def acceptCommands(self):
        print "[INFO] Command service started!"
        while(True):
            #data = self.tcp.recvCommand()
            data = self.udp.nextCommand()
            # print data
            command = data[1]
            # print data
            if command == 'o':
                self.gimbal.set(data[0])
            elif command in self.actions:
                self.actions[command]()
            # print "Data:", self.flight.msp_data()            
                



if __name__ == "__main__":
    app = App("/dev/ttyUSB0","/dev/ttyUSB1",'192.168.0.77',5050,'192.168.137.132',8080)
    # app = App("/dev/ttyUSB0",'localhost',8080)