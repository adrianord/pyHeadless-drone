import time
import threading
from pyMultiwii import MultiWii
import cv2
import numpy as np
from mosse import MOSSE
import math

class Gimbal:
    
    global CENTERX, CENTERY
    CENTERX = 1500
    CENTERY = 1500
    MODE1 = 1200

    def __init__(self,COM_PORT,cam):
        self.board = MultiWii(COM_PORT)
        self.reset()
        self.cam = cam
        self.tracker = None
        t1 = threading.Thread(target=self.run)
        t1.daemon = True
        t1.start()
        
        

    def reset(self):
        self.data = {"x":CENTERX,"y":CENTERY,"Mode":self.MODE1}

    def msp_data(self):
        return [self.data["x"],self.data["y"],self.data["Mode"]]

    def run(self):
        while True:
            self.board.sendCMD(6,MultiWii.SET_MOTOR,self.msp_data())
            time.sleep(0.0035)
    
    def track(self):
        while True:
            frame = self.cam.frame.copy()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            if self.tracker is not None:
                self.tracker.update(frame_gray)
                if self.tracker.good:
                    (x,y) = self.tracker.pos
                    self.center(640/2,480/2,x,y,True)

            
    def set(self, rect):
        frame_gray = cv2.cvtColor(self.cam.frame.copy(), cv2.COLOR_BGR2GRAY)
        self.tracker = MOSSE(frame_gray, rect)

    def center(self,centerX,centerY,x,y,tracking):
        if tracking:

            if abs(centerY - y) > 30:
                mult = 1

                if centerY - y < 0:
                    mult = -1

                self.data["y"] += mult * math.ceil(mult * (float(centerY) - float(y)) / 170.0)
                
            
            if abs(centerX - x) > 25:
                mult = 1

                if centerX - x < 0:
                    mult = -1

                self.data["x"] = 1500 + mult * math.ceil(mult * (float(x) - float(centerX)) * 2.5)

                if self.data["x"] < 1000:
                    self.data["x"] = 1050
                elif self.data["x"] > 2000:
                    self.data["x"] = 1950

                # print("Tracking {},centerX {}, centerY {}, x {}, y {}, data {}, math {}".format(self.tracking,centerX,centerY,x,y,data,mult * math.ceil(mult * (float(centerX) - float(x)) / 10.0)))
            else:
                self.data["x"] = 1500
            
        else:
            self.data["x"] = 1500