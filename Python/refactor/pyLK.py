import time
from threading import Thread
import cv2
import numpy as np
from multiprocessing import Process, Pipe

lk_params = dict( winSize  = (19, 19),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

feature_params = dict( maxCorners = 1000,
                       qualityLevel = 0.01,
                       minDistance = 1,
                       blockSize = 100 )

def checkedTrace(img0, img1, p0, back_threshold = 1.0):
    p1, st, err = cv2.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
    p0r, st, err = cv2.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
    d = abs(p0-p0r).reshape(-1, 2).max(-1)
    status = d < back_threshold
    return p1, status

class LK:

    def __init__(self, cam, flight):
        self.cam = cam
        self.flight = flight
        self.x = 0
        self.y = 0
        self.avgX = 0
        self.avgY = 0
        self.frame_size = (640,480)
        print "[INFO] Starting LK Tracking..."
        t0 = Thread(target=self.run)
        t0.daemon = True
        t0.start()

    def run(self):
        p,c = Pipe()
        proc = Thread(target=self.track, args=(c,))
        proc.daemon = True
        proc.start()
        print "[INFO] Started LK Tracking"
        while True:
            p.send(self.cam.frame.copy())
            # print "PARENT SENT"
            Rx,Ry = p.recv()
            # print "PARENT RECIEVED"
            x, y = self.headlessConversion(Rx,Ry)
            print "x: {} y: {}".format(int(x),int(y))



    def headlessConversion(self, x, y):
        return x,y


    def track(self, pipe):
        p0 = None
        x, y, avgX, avgY = 0,0,0,0
        while True:
            frame = pipe.recv()
            # print "CHILD RECIEVED"
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            if p0 is not None:
                p2, trace_status = checkedTrace(gray1, frame_gray, p1)
                p1 = p2[trace_status].copy()
                p0 = p0[trace_status].copy()
                gray1 = frame_gray
                if len(p0) < 100:
                    p0 = None
                    pipe.send((x,y))
                    # print "CHILD SENT"
                    continue
                H, status = cv2.findHomography(p0, p1, (0, cv2.RANSAC)[0], 10.0)
                sumX = 0
                sumY = 0
                for (x0, y0), (x1, y1), good in zip(p0[:,0], p1[:,0], status[:,0]):
                    if good:
                        sumX += x0 - x1
                        sumY += y1 - y0
                avgX = sumX / len(p0)
                avgY = sumY / len(p0)
            else:
                x += avgX
                y += avgY
                avgX = 0
                avgY = 0
                frame0 = frame.copy()
                p0 = cv2.goodFeaturesToTrack(frame_gray, **feature_params)
                if p0 is not None:
                    p1 = p0
                    gray0 = frame_gray
                    gray1 = frame_gray
            pipe.send((x,y))
            # print "CHILD SENT"