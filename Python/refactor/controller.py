import socket
import threading
import cv2
import numpy as np
from pyTCPServer import TCPServer
from msvcrt import getch
from threading import Thread
import json as pickle
from pyUDP import UDPClient
from common import RectSelector

class App:
    
    title = "Preview"

    def __init__(self,TCP_IP,TCP_PORT,UDP_IP,UDP_PORT):
        self.tcp = TCPServer(TCP_IP,TCP_PORT)
        self.tcp.listen()
        self.udp = UDPClient(UDP_IP,UDP_PORT)
        self.key = '0'
        t0 = Thread(target=self.sendCommands)
        t0.daemon = True
        t0.start()
        t1 = Thread(target=self.recieve)
        t1.daemon = True
        t1.start()
        t0.join()
        # t1.join()

    def sendCommands(self):
        while True:
            self.key = getch()
            print self.key
            if ord(self.key) == 27:
                self.tcp.conn.close()
                break
            data_string = pickle.dumps([[0,0,0,0],self.key])
            # self.udp.sendCommand(str(len(data_string)).ljust(16)) 
            self.udp.sendCommand(data_string)
            # if self.key != '0':
            #     self.key = '0'
    
    def grabKey(self):
        while True:
            self.key = getch()
            print self.key

    def onrect(self, rect):
        x0,y0,x1,y1 = rect
        print rect
        print [x0,y0,x1,y1]
        data_string = pickle.dumps([[int(x0),int(y0),int(x1),int(y1)],'o'])
        self.udp.sendCommand(data_string)

    def recieve(self):
        cv2.namedWindow(self.title)
        self.rect_sel = RectSelector(self.title,self.onrect)
        while True:
            # print "grabbed"
        #    lk = self.tcp.recvframe()
            mosse = self.tcp.recvframe()
            self.rect_sel.draw(mosse) 
        #    cv2.imshow("lk",lk)
            cv2.imshow(self.title,mosse)
            key = cv2.waitKey(1) & 0xFF
        #    self.sendCommands()

if __name__ == "__main__":
    app = App('192.168.0.77',5050,"192.168.137.132",8080)
    # app = App('localhost',8080)