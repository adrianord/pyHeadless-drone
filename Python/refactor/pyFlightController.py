import time
import threading
from pyMultiwii import MultiWii

class FlightController:
    
    global ROLL_D, PITCH_D, YAW_D, THROTTLE_D
    ROLL_D = 1495
    PITCH_D = 1495
    YAW_D = 1500
    THROTTLE_D = 1000
    ROLL = 1
    PITCH = 2
    YAW = 3
    THROTTLE = 4

    def __init__(self,COM_PORT):
        self.board = MultiWii(COM_PORT)
        self.armed = False
        self.reset()

    def arm(self):
        self.armed = True
        self.reset()
        self.board.arm()
        t1 = threading.Thread(target=self.startDrone)
        t1.daemon = True
        t1.start()

    def disarm(self):
        self.armed = False
        self.board.disarm()
        self.reset()

    def reset(self):
        self.data = {"roll":ROLL_D,"pitch":PITCH_D,"yaw":YAW_D,"throttle":THROTTLE_D}

    def msp_data(self):
        return [self.data["roll"],self.data["pitch"],self.data["yaw"],self.data["throttle"]]
    
    def setMspData(self,data):
        self.data["roll"] = data[0]
        self.data["pitch"] = data[1]
        self.data["yaw"] = data[2]
        self.data["throttle"] = data[3]
    
    def setData(self,t,n):
        if t == FlightController.ROLL:
            self.data["roll"] += n
        if t == FlightController.PITCH:
            self.data["pitch"] += n
        if t == FlightController.YAW:
            self.data["yaw"] += n
        if t == FlightController.THROTTLE:
            self.data["throttle"] += n

    def statistics(self,motor,rc,attitude,raw_imu):
        statistics = ""
        if motor:
            self.board.getData(MultiWii.MOTOR)
            statistics += "m1{}\tm2{}\tm3{}\tm4{}\t".format(float(self.board.motor['m1']),float(self.board.motor['m2']),float(self.board.motor['m3']),float(self.board.motor['m4']))
        if rc:
            self.board.getData(MultiWii.RC)
            statistics += "r{}\tp{}\ty{}\tt{}\t".format(float(self.board.rcChannels['roll']),float(self.board.rcChannels['pitch']),float(self.board.rcChannels['yaw']),float(self.board.rcChannels['throttle']))
        if attitude:
            self.board.getData(MultiWii.ATTITUDE)
            statistics += "angx{}\tangy{}\th{}\t".format(float(self.board.attitude['angx']),float(self.board.attitude['angy']),float(self.board.attitude['heading']))
        if raw_imu:
            self.board.getData(MultiWii.RAW_IMU)
            statistics += "aX\t{:.2f}\taY{:.2f}".format(float(self.board.rawIMU['ax']/100),float(self.board.rawIMU['ay']/100))
        return statistics

    def startDrone(self):
        while self.armed:
            self.board.sendCMD(8,MultiWii.SET_RAW_RC,self.msp_data())
            time.sleep(0.02)