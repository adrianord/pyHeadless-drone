import socket
import time
from threading import Thread
from sys import stdout
import sys
from multiprocessing import Pipe, Process
import cv2
import numpy as np
import json as pickle
from functools import partial 

class TCPServer:

    def __init__(self,TCP_IP,TCP_Port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((TCP_IP,TCP_Port))
    
    def listen(self):
        self.sock.listen(True)
        self.conn, self.addr = self.sock.accept()
        print "Connected to {}".format(self.addr)

    def recvframe(self):
        length = self.recvall(self.conn,16)
        stringData = self.recvall(self.conn, int(length))
        data = np.fromstring(stringData, dtype='uint8')
        return cv2.imdecode(data,1)

    def recvall(self, sock, count):
        buf = b''
        while count:
            newbuf = sock.recv(count)
            if not newbuf: return None
            buf += newbuf
            count -= len(newbuf)
        return buf
    
    def sendCommand(self,command):
        self.conn.send(command)

class TCPClient:

    def __init__(self,TCP_IP,TCP_PORT):
        print "[INFO] Starting TCP Services..."
        self.p,c = Pipe()
        Process(target=TCPProcess.run,args=(TCPProcess(),c,TCP_IP,TCP_PORT)).start()
        # self.tcp.daemon = True
        # self.tcp.start()
        print "[INFO] TCP Services Started!"


    def sendFrame(self,frame):
        self.p.send((2,frame))

    def recvCommand(self):
        self.p.send((1,None))
        return self.p.recv()
    

class TCPProcess:

    def run(self, pipe, TCP_IP, TCP_PORT):
        self.pipe = pipe
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((TCP_IP, TCP_PORT))
        while True:
            command,data = self.pipe.recv()
            if command == 1:
                self.recvCommand()
            if command == 2:
                print "[INFO] Sending frame..."
                self.sendFrame(data)
                print "[INFO] Sent frame!"

    def sendFrame(self,frame):
        encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
        result, imgencode = cv2.imencode('.jpg', frame, encode_param)
        data = np.array(imgencode)
        stringData = data.tostring()

        self.sock.send( str(len(stringData)).ljust(16));
        self.sock.send( stringData );

    def recvCommand(self):
        length = self.recvall(self.sock,16)
        data = self.recvall(self.sock,int(length))
        data_loaded = pickle.loads(data)
        self.pipe.send(data_loaded)
    
    def recvall(self, sock, count):
        buf = b''
        while count:
            newbuf = sock.recv(count)
            if not newbuf: return None
            buf += newbuf
            count -= len(newbuf)
        return buf
