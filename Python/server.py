import socket
from pyMultiwii import MultiWii
import time
import threading
from sys import stdout

UDP_IP = '192.168.0.100'
UDP_PORT = 8080
MESSAGE = 'Hello, World!'
serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
print 'listening on port 8080'
serversocket.bind((UDP_IP,UDP_PORT))
global running
running = True
global armed
armed = False
global data
data = [1500,1500,1500,1000]
global message
message = ''
print 'data is: ' + str(data)

board = MultiWii("/dev/ttyUSB0")


def Command(message):
    global armed
    global data
    global running
    if((message == 'ARM') & (armed == False)):
        try:
            board.arm()
            armed = True
            t2 = threading.Thread(target=AcceptCommands)
            t2.daemon = True
            t2.start()
        except Exception,error:
            print "Error on Main: "+str(error)
    elif((message == 'DISARM') & (armed == True)):
        try:
            Reset()
            armed = False
            board.disarm()
        except Exception,error:
            print "Error on Main: "+str(error)
    elif((message == 'T+') & (armed) & (data[3] < 2000)):

        data[3] += 10
    elif((message == 'T-') & (armed) & (data[3] > 1000)):
        data[3] -= 10
    elif((message == 'Y+')&(armed) & (data[2] < 2000)):
        data[2] += 10
    elif((message == 'Y-')&(armed)& (data[2] > 1000)):
        data[2] -= 10
    elif((message == 'P+')&(armed) & (data[1] < 2000)):
        data[1] += 1
    elif((message == 'P-')&(armed) & (data[1] > 1000)):
        data[1] -= 1
    elif((message == 'R+')&(armed) & (data[0] < 2000)):
        data[0] += 1
    elif((message == 'R-')&(armed) & (data[0] > 1000)):
        data[0] -= 1
    elif(message == 'KILL'):
        Reset()
        armed = False;
        running = False;
        board.disarm()

def AcceptCommands():
    global data
    global armed
    while armed :
        board.sendCMD(8,MultiWii.RC,data)
        time.sleep(0.09)

def Reset():
    global data
    data = [1500,1500,1500,1000]
    

def ShowStatistics():
    global running
    global message
    while running :
        board.getData(MultiWii.MOTOR)
        statistics = "m1 = {} m2 = {} m3 = {} m4 = {} ".format(float(board.motor['m1']),float(board.motor['m2']),float(board.motor['m3']),float(board.motor['m4']))
        board.getData(MultiWii.RC)
        statistics += "r = {:+.0f} p = {:+.0f} y = {:+.0f} t = {:+.0f} ".format(float(board.rcChannels['roll']),float(board.rcChannels['pitch']),float(board.rcChannels['yaw']),float(board.rcChannels['throttle']))
        board.getData(MultiWii.ATTITUDE)
        statistics += "angx: {:+.2f} angy: {:+.2f} h: {} ".format(float(board.attitude['angx']),float(board.attitude['angy']),float(board.attitude['heading']))
        board.getData(MultiWii.RAW_IMU)
        statistics += "aX: {:+.2f} aY: {:+.2f} ".format(float(board.rawIMU['ax']),float(board.rawIMU['ay']))
        statistics += "armed: " + str(armed) + " last message: " + message + '    '
        stdout.write("\r%s" % statistics )
        stdout.flush()
  
t1 = threading.Thread(target=ShowStatistics)
t1.start()    
while running == True:
    # print '\nwaiting to receive message'
    message, addr = serversocket.recvfrom(1024) # buffer size is 1024 bytes
    # print 'received "%s" bytes from "%s"' %(len(message), addr)
    # print "received message:", message
    Command(message)
