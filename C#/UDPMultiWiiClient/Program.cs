﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPMultiWiiClient
{
    class Program
    {
        static void Main(string[] args)
        {
            UDPTest();
        }

        static void TCPTest()
        {
            TcpClient client = new TcpClient();
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5005);
            string message = "";
            client.Connect(ep);
            using (NetworkStream stream = client.GetStream())
            {
                while (message != "exit")
                {
                    message = Console.ReadLine();
                    byte send = 1;
                    byte[] data = {send};
                    stream.Write(data, 0, data.Length);
                    Console.WriteLine($"Sent: {message} size: {data.Length}");
                    data = new byte[256];
                    string response = string.Empty;
                    Int32 bytes = stream.Read(data, 0, data.Length);
                    Console.WriteLine($"Recieved: {bytes}");
                }
            }
            client.Close();
        }

        static void UDPTest()
        {
            UdpClient client = new UdpClient();
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("192.168.0.100"), 8080);
            client.Connect(ep);
            string message = "This is my message";
            byte[] rawMessage = Encoding.ASCII.GetBytes(message);
            int count = 0;
            ConsoleKey lastKey = ConsoleKey.Escape;
            ConsoleKey key;
            while ((key = Console.ReadKey(true).Key) != ConsoleKey.Escape)
            {
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        message = "P+";
                        break;
                    case ConsoleKey.DownArrow:
                        message = "P-";
                        break;
                    case ConsoleKey.LeftArrow:
                        message = "R-";
                        break;
                    case ConsoleKey.RightArrow:
                        message = "R+";
                        break;
                    case ConsoleKey.PageUp:
                        message = "T+";
                        break;
                    case ConsoleKey.PageDown:
                        message = "T-";
                        break;
                    case ConsoleKey.A:
                        message = "ARM";
                        break;
                    case ConsoleKey.OemPeriod:
                        message = "Y+";
                        break;
                    case ConsoleKey.OemComma:
                        message = "Y-";
                        break;
                    case ConsoleKey.D:
                        message = "DISARM";
                        break;
                    case ConsoleKey.E:
                        message = "KILL";
                        break;
                    case ConsoleKey.B:
                        message = "BUZZ";
                        break;

                }
                if (key == lastKey)
                    count++;
                else
                {
                    count = 0;
                    lastKey = key;
                }
                rawMessage = Encoding.ASCII.GetBytes(message);
                client.Send(rawMessage, rawMessage.Length);
                //                byte[] rawReturn = client.Receive(ref ep);
                Console.WriteLine("Sending message: " + message + "              ");
                Console.SetCursorPosition(0, 1);
                Console.WriteLine("Repeat: " + count + "           ");
                Console.SetCursorPosition(0, 0);
            }
            Console.WriteLine("Closing connection");
            client.Close();
        }
    }
}
