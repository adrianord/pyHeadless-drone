#!/usr/bin/env python

"""test-attitude.py: Main module to test communication with a MultiWii Board by asking attitude."""

__author__ = "Aldo Vargas"
__copyright__ = "Copyright 2014 Aldux.net"

__license__ = "GPL"
__version__ = "1"
__maintainer__ = "Aldo Vargas"
__email__ = "alduxvm@gmail.com"
__status__ = "Development"

from pyMultiwii import MultiWii
from sys import stdout
import time
import threading

def arm(board):
    try:
        board.arm()
        print "Board is armed now!"
        print "In 3 seconds it will disarm..."
        
        time.sleep(3)
        board.disarm()
        print "Disarmed."
        time.sleep(3)
    except Exception,error:
        print "Error on Main: "+str(error)


def read(board):
    if __name__ == "__main__":

        #board = MultiWii("/dev/ttyUSB0")
        try:
            counter = 0;
            # while counter < 100:
            #     board.getData(MultiWii.RC)
            #     #print board.attitude #uncomment for regular printing

            #     # Fancy printing (might not work on windows...)
            #     message = "roll = {:+.2f} \t pitch = {:+.2f} \t yaw = {:+.2f} \t throttle = {:+.4f} \t counter = {:+.1f}".format(float(board.rcChannels['roll']),float(board.rcChannels['pitch']),float(board.rcChannels['yaw']),float(board.rcChannels['throttle']),counter)
            #     stdout.write("\r%s" % message )
            #     stdout.flush()
            #     counter+=1
            board.arm()
            print "\nBoard is armed now!"
            print "In 3 seconds it will disarm..."
            counter = 0
            

            while counter < 100:
                board.getData(MultiWii.MOTOR)
                #print board.attitude #uncomment for regular printing

                # Fancy printing (might not work on windows...)
                message = "motor1 = {:+.2f} \t motor2 = {:+.2f} \t motor3 = {:+.2f} \t motor4 = {:+.4f} \t".format(float(board.motor['m1']),float(board.motor['m2']),float(board.motor['m3']),float(board.motor['m4']))
                board.getData(MultiWii.RC)
                message += "roll = {:+.2f} \t pitch = {:+.2f} \t yaw = {:+.2f} \t throttle = {:+.4f} \t counter = {:+.1f}".format(float(board.rcChannels['roll']),float(board.rcChannels['pitch']),float(board.rcChannels['yaw']),float(board.rcChannels['throttle']),counter)
                stdout.write("\n" + message )
                stdout.flush()
                counter+=1
                # End of fancy printing
            counter = 0;
            count = 0
            # while count < 15:
            while counter < 100:
                data = [1500,1500,1500,1800]
                board.sendCMD(8,MultiWii.SET_RAW_RC,data)
                time.sleep(0.05)
                message = "motor1 = {:+.2f} \t motor2 = {:+.2f} \t motor3 = {:+.2f} \t motor4 = {:+.4f} \t".format(float(board.motor['m1']),float(board.motor['m2']),float(board.motor['m3']),float(board.motor['m4']))
                board.getData(MultiWii.RC)
                message += "roll = {:+.2f} \t pitch = {:+.2f} \t yaw = {:+.2f} \t throttle = {:+.4f} \t counter = {:+.1f}".format(float(board.rcChannels['roll']),float(board.rcChannels['pitch']),float(board.rcChannels['yaw']),float(board.rcChannels['throttle']),counter)
                stdout.write("\n" + message )
                stdout.flush()
                counter+=1
                # count+=1
            counter = 0;
            board.disarm()
            print "\nDisarmed."
            time.sleep(3)
            counter = 0
            while counter < 100:
                board.getData(MultiWii.MOTOR)
                #print board.attitude #uncomment for regular printing

                # Fancy printing (might not work on windows...)
                message = "motor1 = {:+.2f} \t motor2 = {:+.2f} \t motor3 = {:+.2f} \t motor4 = {:+.4f} \t".format(float(board.motor['m1']),float(board.motor['m2']),float(board.motor['m3']),float(board.motor['m4']))
                board.getData(MultiWii.RC)
                message += "roll = {:+.2f} \t pitch = {:+.2f} \t yaw = {:+.2f} \t throttle = {:+.4f} \t counter = {:+.1f}".format(float(board.rcChannels['roll']),float(board.rcChannels['pitch']),float(board.rcChannels['yaw']),float(board.rcChannels['throttle']),counter)
                stdout.write("\n"  + message )
                stdout.flush()
                counter+=1
                # End of fancy printing
        except Exception,error:
            print "Error on Main: "+str(error)

board = MultiWii("COM5")
finished = False
t1 = threading.Thread(target=read(board))
t1.start()
finished = True
